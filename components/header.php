<?php
  function printHeaderLink($name, $label) {
    echo "<a onclick=\"return openHeaderLink(event, '$name', this)\" href='/" . $name . "' >" . $label . "</a>";
  }
?><header>
  <strong>
    <a onclick="return openHeaderLink(event, 'home', this)" href='/' title='Home' >
      <span><span class='hover'><span class='neon'>Nicolas</span></span><span class='nothover'><span class='neon'>Colisan</span></span></span>
      <span><span class='hover'><span class='inox'>Violette</span></span><span class='nothover'><span class='inox'>Loveteit</span></span></span>
    </a>
  </strong>
  <nav onclick='toggleOpen(this)'>
    <ul>
      <li>
        <?=printHeaderLink('portfolio', 'Portfolio') ?>
      </li>
      <li>
        <?=printHeaderLink('cv', 'CV') ?>
      </li>
      <li>
        <?=printHeaderLink('files', 'Files') ?>
      </li>
      <li>
        <?=printHeaderLink('contact', 'Contact') ?>
      </li>
    </ul>
  </nav>
</header>
<script>
  function toggleOpen(elt) {
    elt.classList.toggle('open')
  }

  function openHeaderLink(evt, name, elt) {
    evt.preventDefault();
    openPage(name);
    window.history.pushState("", "", elt.href);
    return false;
  }
</script>