<?php
  include_once('common.php');

  if (isset($_GET['page'])) {
    $sanitizedPage = $_GET['page'];
    $sanitizedPage = str_replace('.', '', $sanitizedPage);
    $sanitizedPage = str_replace('/', '', $sanitizedPage);
    $sanitizedPage = str_replace('\\', '', $sanitizedPage);
    if (is_file('pages/' . $sanitizedPage . '.php'))
      include('pages/' . $sanitizedPage . '.php');
    else
      include('pages/404.php');
  }