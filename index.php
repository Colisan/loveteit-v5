<?php
  include_once('common.php');

  if (isset($_GET['page'])) {
    $page = NULL;
    foreach(scandir('pages') as $file) {
      if ($file[0] !== '.') {
        $pageName = substr($file, 0, strlen($file) - 4);
        if ($pageName === $_GET['page']) $page = $pageName;
        else if ($pageName === '/' . $_GET['page']) $page = $pageName;

        if($page === 'files') {
          $page .= $_GET['path'];
        }
      }
    }
    if ($page === NULL)
      $page = '404';
  } else {
    $page = 'home';
  }

?><!DOCTYPE html>
<html>
  <head>
    <title>Colisan Loveteit</title>
    <link href='https://fonts.googleapis.com/css?family=Permanent+Marker|Share+Tech|Righteous&display=swap' rel='stylesheet'> 
    <link rel='stylesheet' href='/assets/style.css'>
    <script src='/assets/common.js'></script>
    <script>
      document.addEventListener('DOMContentLoaded', () => {
        openPage("<?=$page ?>")
      })
    </script>
  </head>
  <body>
    <div id='main'>
      <?php include("components/header.php"); ?>
      <div id='page'><div id='loading'></div></div>
    </div>
  </body>
</html>