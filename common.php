<?php
  include_once('config.php');
  /*
  $LANGUAGES = ['en', 'fr'];

  function fetchLang(){
    global $LANG;
    global $LANGUAGES;
    
    if (isset($_GET['lang'])) {
      if (in_array($_GET['lang'], $LANGUAGES))
        $_SESSION['lang'] = $LANG = $_GET['lang'];
    }
    else {
      $userLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
      $LANG = isset($_SESSION['lang'])
        ? $_SESSION['lang']
        : (in_array($userLang, $LANGUAGES)
          ? $userLang
          : $LANGUAGES[0]
        );
    }
  }

  function connectBDD(){
    global $DB;
    global $CONFIG;

    try {
      $DB = new PDO("mysql:dbname=loveteit_v5;host=127.0.0.1",
        $CONFIG->bduser,
        $CONFIG->bdpass,
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (PDOException $e) {
      initBDD();
    }
  }

  function initBDD(){
    global $DB;
    global $CONFIG;

    try {
      $tdb = new PDO("mysql:host=127.0.0.1",
        $CONFIG->bduser,
        $CONFIG->bdpass,
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $script = file_get_contents("init_db.sql");
      $tdb->exec($script);
      $tdb->query('KILL CONNECTION_ID()');
      $tdb = NULL;
      connectBDD();
    } catch (PDOException $e) {
      var_dump($e);
    }
  }
  
  function getTranslation($code, $targetLang = NULL){
    global $DB;
    global $LANG;
    static $cached = [];

    if ($targetLang === NULL) { $targetLang = $LANG; }

    $defaultReturn = '[' . strtoupper($targetLang) . ';' . $code . ']';

    if (isset($cached[$code])) {
      return $cached[$code][$targetLang] !== NULL ? $cached[$code][$targetLang] : $defaultReturn;
    }

    $prep = $DB->prepare("SELECT * FROM translation WHERE code=:code");
    $prep->execute(['code' => $code]);
    if ($prep) {
      $res = $prep->fetch();
      if ($res && $res['code'] === $code) {
        $cached[$code] = $res;
        return $res[$targetLang] !== NULL ? $res[$targetLang] : $defaultReturn;
      }
    }

    $prep = $DB->prepare("INSERT INTO translation (code) VALUES (:code)");
    $prep->execute(['code' => $code]);
    
    return $defaultReturn;
  }*/

  function toto($length = 1) {
    for ($i = 0; $i < $length; $i++) {
      $nbS = random_int(10, 20);
      echo "<p>";
      for ($j = 0; $j < $nbS; $j++) {
        echo "Toto ";
        $nbW = random_int(10, 20);
        for ($k = 0; $k < $nbW; $k++) {
          $nbT = random_int(1, 2) * random_int(1, 3);
          for ($l = 0; $l < $nbT; $l++) {
            echo ['to', 'ro', 'po'][random_int(0, 2)];
          }
          echo ($k != $nbW - 1 ? ' ' : '');
        }
        echo "." . ($j != $nbS - 1 ? ' ' : '');
      }
      echo ($i != $length - 1 ? '<br><br>' : '');
      echo "</p>";
    }
  }