<?php
    header('Access-Control-Allow-Origin: *');
    
    function aes_decrypt($val,$key) {
        $mode=MCRYPT_MODE_ECB;    
        $enc=MCRYPT_RIJNDAEL_128;
        $val=hex2bin($val);
        return mcrypt_decrypt($enc, $key, $val, $mode, mcrypt_create_iv( mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM) ); 
    }

    if (isset($_GET['datas'])) {
    
        $res = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', aes_decrypt($_GET['datas'], "chkoiegyihssacno"));
        if ($res) {
            $obj = json_decode($res);
            $obj->player = trim($obj->player);
            $obj->player = $obj->player == "" ? "naan" : $obj->player;
            $obj->password = $_SERVER['REMOTE_ADDR'];

            $BDD = new PDO('mysql:host=localhost;dbname=highscores', 'highscore', 'webhighbdpass');
        
            $datas = $BDD->prepare("SELECT * FROM scores WHERE game=:game AND player=:player");
            $datas->execute(array("game" => $obj->game, "player" => $obj->player));
            $datas = $datas->fetch();

            if ($datas){
                if (intval($obj->score) <= intval($datas['score'])) {
                    echo "{\"status\":\"ok\"}";
                }
                else 
                {
                    $req = $BDD->prepare("UPDATE scores SET score = :score, date = CURRENT_TIMESTAMP WHERE game=:game AND player=:player AND score<:score AND password=:password");
                    $req->execute((array) $obj);

                    if ($req->rowCount() > 0) {
                        echo "{\"status\":\"ok\"}";
                    }
                    else {
                        echo "{\"status\":\"ko pass\"}";
                    }
                }
            }
            else {
                $req = $BDD->prepare("INSERT INTO scores (game, player, password, score) VALUES(:game, :player, :password, :score)");
                $req->execute((array) $obj);

                echo "{\"status\":\"ok\"}";
            }
        }
    }
