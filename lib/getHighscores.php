<?php
    header('Access-Control-Allow-Origin: *');

    $BDD = new PDO('mysql:host=localhost;dbname=highscores', 'highscore', 'webhighbdpass');
    
    $datas = $BDD->prepare("SELECT player, score FROM scores WHERE game=:game ORDER BY score DESC");
    $datas->execute(array('game' => $_GET['game']));
    $datas = $datas->fetchAll(PDO::FETCH_OBJ);
    
    $first = true;
    foreach ($datas as $data) {
        echo ($first ? "" : "|") . str_replace('|', 'I', $data->player) . "|" . $data->score;
        $first = false;
    }
