
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `loveteit_v5` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `loveteit_v5`;

CREATE TABLE IF NOT EXISTS `translation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `fr` text COLLATE utf8mb4_bin,
  `en` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
COMMIT;


CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title_fr` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `fr` text COLLATE utf8mb4_bin,
  `en` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
COMMIT;


COMMIT;