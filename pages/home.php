<script src='/assets/lib/babylon.js'></script>
<script src='/assets/lib/babylon.gridMaterial.js'></script>
<script src='/assets/lib/perlin.js'></script>

<script src='/assets/lib/require.js'></script>
<script src='/assets/page_home/ts/build/TerrainGenerator.js'></script>
<script src='/assets/page_home/script.js'></script>

<div id='home'>
  <div id='loading'></div>
  <canvas id="home_canvas"></canvas>
  <div id='canvas_blend'></div>
</div>