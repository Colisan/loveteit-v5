<?php
  include_once("common.php");

  function printGame($title, $subtitle, $bgImg, $link, $text, $leftSided, $award=false, $additionalClasses='') {
    $orientationClass = $leftSided ? "portfolio-wrapper_leftSided" : "portfolio-wrapper_rightSided";

    echo <<<HTML
    <div class='portfolio-wrapper $orientationClass $additionalClasses'>
      <div class='portfolio-desc'>
        <div class='portfolio-title'>
          $title
        </div>
        <div class='portfolio-subtitle'>
          $subtitle
        </div>
        <div class='portfolio-text'>
          $text
        </div>
      </div>
      <img class='portfolio-img' src='/assets/img/$bgImg'>
HTML;
    if ($link)
      echo "<a href='$link' target='_blank' class='portfolio-button'></a>";

    if ($award)
      echo "<div class='portfolio-award'></div>";
      
    echo "</div>";
  }

?><div id='portfolio'>
  <div id='content'>
    <?php 
      printGame(
        "Cupidown",
        "Made in June 2020",
        "cupidown.gif",
        "https://alakajam.com/scorespace-alakajam/955/cupidown/",
        "Cupid lost his bow! You'll have to make people fall in love by letting arrow fall under you.<br>
        <br>
        An arcade platformer scoring game made alone in 72h, all from scratch, for the <a href='https://alakajam.com/scorespace-alakajam' target='_blank'>ScoreSpace×Alakajam!</a> gamejam.<br>
        <br>
        My goal for this game was to create a joyful atmosphere, to allow players to express their creativity in the way they prefere to play, and to design both a smooth learning curve and a fast back-in-action for experienced players.<br>
        I also challenged myself to implement a Twitch API connexion with live chat interaction!<br>
        ",
        true
      );
      
      printGame(
        "Cisaw Seercus",
        "Made in April 2020",
        "cisawSeercus.gif",
        "https://colisan.itch.io/cisaw-seercus",
        "Two elves got captured by an evil monster circus. Keep the show going by catching juggler balls... and staying alive!<br>
        <br>
        A exigent platformer scoring game made from scratch by myself in only 72h, for the 8th <a href='https://www.scorespace.net/#/' target='_blank'>ScoreSpace Gamejam</a>.<br>
        My goal for this game was to infuse the theme into the core gameplay and try to come up to something original.<br>
        <br>
        Winning award from the streaming community!<br>
        <br>
        Played in a 72h competion for the highscore (won by <a href='https://www.twitch.tv/silentdrumr' target='_blank'>SilentDrumr</a> with a huge score of 1148)!<br>
        ",
        false,
        true
      );

      printGame(
        "Lock Apala",
        "Made in August 2019",
        "lockApala.gif",
        "https://colisan.itch.io/lock-apala",
        "Collect good and bad karma from wandering souls and deposite them in the heavens fountains.<br>
        Each karma type has its own fountain, and you can't hold both at the same time, so plan ahead and choose wisely.<br>
        <br>
        A hard platformer puzzle scoring game made entirely (design, gameplay, art and music) by me, in 72h, for the 6th <a href='https://www.scorespace.net/#/' target='_blank'>ScoreSpace Gamejam</a>.<br>
        My aim for this game was to find a unique, clever and fun gameplay while staying on the good track I was on with the previous gamejam.<br>
        <br>
        Winning award from the streaming community!<br>
        <br>
        Played and fought on for the highscore during a 72h competition (won by <a href='https://mixer.com/Natebag' target='_blank'>Natebag</a> with a score of 304)!<br>
        Featured on the <a href='https://youtu.be/MpHi8PNAzDA?t=156' target='_blank'>2019 All Star</a> ScoreSpace video.
        ",
        true,
        true
      );
      
      printGame(
        "Mine Hazards",
        "Made in Mars 2019",
        "mineHazards.gif",
        "https://colisan.itch.io/mine-hazards",
        "Avoid the collapsing mine's many hazards, and break blocks to collect shiny gems... while wall-jumping like a madman!<br>
        <br>
        A challenging platformer puzzle scoring game all made from scratch by myself in 72h, for the 3rd <a href='https://www.scorespace.net/#/' target='_blank'>ScoreSpace Gamejam</a>.<br>
        My goal for this game was make the player feel entierly responsible for his defeat, and to provide a feeling of progress and difficulty escalation.<br>
        <br>
        Won the 1st place in Overall and Gameplay categories!<br>
        <br>
        Played in a 72h competion for the highscore (won by <a href='https://www.twitch.tv/goodkenneth' target='_blank'>GoodKenneth</a> with a score of 525)!<br>
        Also featured on the <a href='https://youtu.be/MpHi8PNAzDA' target='_blank'>2019 All Star</a> ScoreSpace video.
        ",
        false,
        true
      );
      
      printGame(
        "Cemetery Chore",
        "Made in 2017",
        "cemeteryChore.gif",
        "https://ldjam.com/events/ludum-dare/40/cemerery-chore",
        "Clean the backyard cemetery from a dangerous zombie horde, and try not to get too corrupted by them before you come back to the monastery.<br>
        <br>
        A fighting plateformer game made entierly by hand, alone, in 48h, for the 40th <a href='https://ldjam.com/' target='_blank'>Ludum Dare Gamejam</a>.<br>
        My aims for this game was to try myself at pixelart, to make the player's movements feel good, and to create a story.<br>
        ",
        true
      );
			
      printGame(
        "Tetris Crush Saga",
        "Made in 2018",
        "tetrisCrush.gif",
        "https://ldjam.com/events/ludum-dare/41/tetris-crush-saga",
        "Stack up some tetrominos, try to make packs with their colored parts, and use your mouse cursor to break them... and watch with delight as your cascading combos hurl your score into infinity and beyond.<br>
        <br>
        A little arcade puzzle game made in less than 48h, 100% from nothing, all by me, for the 41th <a href='https://ldjam.com/' target='_blank'>Ludum Dare Gamejam</a>.<br>
        My goal for this game was to work on replayability by moving outside my previously usual story-driven games.
        ",
        false,
        false,
        'portfolio-wrapper_moreDark'
			);
			
      printGame(
        "And many unfinished gamejam...",
        "Since 2012 !",
        "others.gif",
        "",
        "Creating games since I was 10 years old... and still working on it " . (intval(date('Y')) - 1995 - 10) . " years later, day to day, getting better and better at making them!<br>
        <br>
        If you really want to see some very old games I made when I was a teenager, you can check <a href='http://ludumdare.com/compo/ludum-dare-26/?action=preview&uid=14410' target='_blank'>Escape</a> for the 26th Ludum Dare, or <a href='http://ludumdare.com/compo/ludum-dare-24/?action=preview&uid=14410' target='_blank'>Gr0wth</a> for the 24th. But don't judge them too harshly! =)
        ",
        true
      );
    ?>
    <div class='portfolio-filler'>
    </div>
  </div>
</div>
