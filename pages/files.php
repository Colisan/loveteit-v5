<?php
  
  $folder = $_GET['path'];
  $folder = str_replace('.', '', $folder);
  $folder = "files" . $folder;

  if (!is_dir($folder)) {
    $folder = "files/";
  }

  $re = '/(.*\/)(\w+\/?)/m';
  $subst = '$1';
	$parentFolder = preg_replace($re, $subst, $folder);
	
	function fileSorter($a, $b) {
		if (strtolower($a) == strtolower($b)) return 0;
		return strtolower($a) < strtolower($b) ? -1 : 1;
	}

  $files = scandir($folder);
  $files = array_filter($files, function ($e) { return $e[0] !== '.'; });
  $subFolders = array_filter($files, function ($e) { global $folder; return filetype($folder . $e) === "dir"; });
	uasort($subFolders, "fileSorter");
	$files = array_filter($files, function ($e) { global $folder; return filetype($folder . $e) === "file"; });
	uasort($files, "fileSorter");

  function printFolder($folderName) {
    global $folder;
    $path = $folder . $folderName . '/';
    echo "<li><a href='/$path' onclick=\"openPage('$path'); return false;\" class='folderView-folder'>$folderName</a></li>";
  }

  function printFile($fileName) {
		global $folder;
		$isImage = strtolower(substr($fileName, -4)) === '.png'
		        || strtolower(substr($fileName, -4)) === '.jpg'
						|| strtolower(substr($fileName, -4)) === '.gif';
		$style = "";
		if ($isImage) {
			$style .= "background-image: url('/$folder$fileName');";
		}
    echo "<li><a style=\"$style\" href='/$folder$fileName' target='_blank' class='folderView-file'>$fileName</a></li>";
  }

?><div id='files'>
  <div id='content'>
    <div id='folderView'>
    <header>
      <a class='folderView-backBtn' href='/<?php echo $parentFolder; ?>' onclick="openPage('<?php echo $parentFolder; ?>'); return false;" ></a>
      <div class='folderView-path'><?php echo $folder ?></div>
      <div class='decorBtn'>
        <div class='decorBtn-dot'></div>
        <div class='decorBtn-dot'></div>
        <div class='decorBtn-dot'></div>
      </div>
    </header>
    <ol>
      <?php
        foreach($subFolders as $subFolder)
          printFolder($subFolder);
        foreach($files as $file)
          printFile($file);
      ?>
    </ol>
    </div>
  </div>
</div>