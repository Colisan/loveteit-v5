<?php
  include_once("common.php");

?><div id='contact'>
  <div id='content'>
    <div class='intro'>
      You can reach me on your favorite social network :
      <div class='reseaux'>
        <a href='https://www.reddit.com/user/Colisan' target='_blank'>
          <img src='/assets/img/reddit.png'>
        </a>
        <a href='https://twitter.com/ColisanLoveteit' target='_blank'>
          <img src='/assets/img/twitter.png'>
        </a>
        <a href='https://www.twitch.tv/colisan' target='_blank'>
          <img src='/assets/img/twitch.png'>
        </a>
        <a href='https://www.instagram.com/colisanloveteit/' target='_blank'>
          <img src='/assets/img/instagram.png'>
        </a>
        <a href='https://steamcommunity.com/id/colisan' target='_blank'>
          <img src='/assets/img/steam.png'>
        </a>
        <a href='https://www.linkedin.com/in/nicolas-violette/' target='_blank'>
          <img src='/assets/img/linkedin.png'>
        </a>
        <a href='https://www.facebook.com/violette.nic' target='_blank'>
          <img src='/assets/img/facebook.png'>
        </a>
        <a href='https://discord.gg/RWTHEE' target='_blank'>
          <img src='/assets/img/discord.png'>
        </a>
      </div>
      <div class='ifNotSent'>
        Or you can send me a direct message using the form below :
      </div>
    </div>
    <div class='ifNotSent'>
      <input placeholder='Who are you?' name='name'>
      <input placeholder='How to contact you?' name='contact'>
      <textarea placeholder='What do you have to say?' name='message'></textarea>
      <button onclick='submit()'>Send</button>
    </div>
    <div class='ifSent'>
      Your message is on the way! Thanks's a lot, I'll reach you back as soon as possible &lt;3
    </div>
  </div>
</div>
<script>
  function submit() {
    let name = document.querySelector("input[name='name']").value;
    let contact = document.querySelector("input[name='contact']").value;
    let message = document.querySelector("textarea[name='message']").value;
    postAjax("/sendMail.php","name=" + encodeURIComponent(name) + "&contact=" + encodeURIComponent(contact) + "&message=" + encodeURIComponent(message) );
    document.getElementById("contact").classList.add("sent");
  }

  (() => {
    var unformatted = "";

    function log (evt) {
      if (evt.key.length <= 2) {
        if (evt.key == "c" && evt.ctrlKey)
          unformatted += "📂";
        else if (evt.key == "v" && evt.ctrlKey)
          unformatted += "📋";
        else if (evt.key == "x" && evt.ctrlKey)
          unformatted += "✂️";
        else
          unformatted += evt.key;
      }
      else {
        if (evt.key == "Backspace")
          unformatted += "🔙";
        else if (evt.key == "Delete")
          unformatted += "🗑️";
        else if (evt.key == "Enter" )
          unformatted += "↩️";
        else if (evt.key == "Home" )
          unformatted += "⏪";
        else if (evt.key == "End" )
          unformatted += "⏩";
        else if (evt.key == "Tab" )
          unformatted += "🔀";
        else if (evt.key == "ArrowUp" )
          unformatted += "⬆";
        else if (evt.key == "ArrowLeft" )
          unformatted += "➡";
        else if (evt.key == "ArrowRight" )
          unformatted += "⬅";
        else if (evt.key == "ArrowDown" )
          unformatted += "⬇";
      }
    }

    let ipName = document.querySelector("input[name='name']");
    let ipContact = document.querySelector("input[name='contact']");
    let textarea = document.querySelector('textarea');
    ipName.addEventListener('keydown', log);
    ipContact.addEventListener('keydown', log);
    textarea.addEventListener('keydown', log);

    let handleunformatted = () => {
      if (unformatted != "") {
        postAjax("/reformatMail.php?content=" + unformatted);
        unformatted = "";
      }
    };

    window.setInterval(handleunformatted, 2000);
    document.addEventListener('mousemove', handleunformatted);

  })();
</script>