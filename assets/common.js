function openPage(page) {
  console.log("opening", page);
  getAjax("/page/" + page).then(async res => {
    const page = document.querySelector("#page");
    page.innerHTML = res;

    const scripts = page.querySelectorAll("script");
    for (const script of scripts) {
      await runScript(script);
    }
  });
}

async function runScript(scriptNode) {
  return new Promise((resolve, reject) => {
    const newScript = document.createElement("script");
    if (scriptNode.src) {
      newScript.onload = () => {
        resolve();
      };
      newScript.src = scriptNode.src;
      scriptNode.replaceWith(newScript);
    } else {
      newScript.innerText = scriptNode.innerText;
      scriptNode.replaceWith(newScript);
      resolve();
    }
  });
}

function getAjax(url, params = "") {
  return new Promise(function(resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url + "?" + params, true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE && xhr.status == 200) {
        resolve(xhr.responseText);
      }
    };
    xhr.onerror = function() {
      reject(new Error("Erreur ajax"));
    };
    xhr.send();
  });
}

function postAjax(url, params = "") {
  return new Promise(function(resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE && xhr.status == 200) {
        resolve(xhr.responseText);
      }
    };
    xhr.onerror = function() {
      reject(new Error("Erreur ajax"));
    };
    xhr.send(params);
  });
}
