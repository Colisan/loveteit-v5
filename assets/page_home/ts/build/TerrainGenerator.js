"use strict";

var _noisejs = _interopRequireDefault(require("noisejs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var HillyValleyGenerator = /*#__PURE__*/function () {
  function HillyValleyGenerator() {
    _classCallCheck(this, HillyValleyGenerator);

    _defineProperty(this, "seed", Math.random() * 65565);

    _defineProperty(this, "noiseLib", new _noisejs["default"]());
  }

  _createClass(HillyValleyGenerator, [{
    key: "setSeed",
    value: function setSeed(newSeed) {
      this.seed = newSeed;
    }
  }, {
    key: "getHeightAt",
    value: function getHeightAt(x, y, time) {
      var road = 1 - Math.cos(Math.PI * x / 50);
      var simplexOctave0 = this.noiseLib.simplex2((x + time) / 30, (y + time) / 30) + 1;
      var simplexOctave1 = this.noiseLib.simplex2(x / 30, y / 30) + 1;
      var simplexOctave2 = this.noiseLib.simplex2(x / 100, y / 100) + 1;
      var height = (road + 0.25) * simplexOctave0 * 2 + road * simplexOctave1 * 2 + road * simplexOctave2 * 7;
      return 0;
    }
  }]);

  return HillyValleyGenerator;
}();