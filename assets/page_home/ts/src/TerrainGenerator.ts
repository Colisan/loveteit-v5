import Noise from "noisejs";
requirejs(["noisejs"]
interface ProceduralTerrainGenerator {
  setSeed(seed: number): void;
  getHeightAt(x: number, y: number, time: number): number;
}

class HillyValleyGenerator implements ProceduralTerrainGenerator {
  private seed = Math.random() * 65565;
  private noise: Noise;

  public constructor() {
    this.noise = new Noise();
  }

  public setSeed(newSeed: number): void {
    this.seed = newSeed;
    this.noise.seed(newSeed);
  }

  public getHeightAt(x: number, y: number, time: number): number {
    const road = 1 - Math.cos((Math.PI * x) / 50);
    const simplexOctave0 =
      this.noise.simplex2((x + time) / 30, (y + time) / 30) + 1;
    const simplexOctave1 = this.noise.simplex2(x / 30, y / 30) + 1;
    const simplexOctave2 = this.noise.simplex2(x / 100, y / 100) + 1;
    const height =
      (road + 0.25) * simplexOctave0 * 2 +
      road * simplexOctave1 * 2 +
      road * simplexOctave2 * 7;

    return 0;
  }
}
