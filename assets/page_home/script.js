(() => {
  class Render {
    constructor() {
      this.canvas = document.getElementById("home_canvas"); // Get the canvas element
      this.engine = new BABYLON.Engine(this.canvas, true); // Generate the BABYLON 3D engine

      // Create the scene space
      this.scene = new BABYLON.Scene(this.engine);

      // Add a camera to the scene and attach it to the canvas
      // Creates, angles, distances and targets the camera
      this.camera = new BABYLON.ArcRotateCamera(
        "Camera",
        0,
        0,
        0,
        new BABYLON.Vector3(0, 0, -100),
        this.scene
      );
      this.zoom = 7;

      // This positions the camera
      this.camera.setPosition(new BABYLON.Vector3(0, 10, 70));

      // Add lights to the scene
      let light1 = new BABYLON.HemisphericLight(
        "light1",
        new BABYLON.Vector3(1, 1, 0),
        this.scene
      );

      // Add and manipulate meshes in the scene

      var ground = BABYLON.MeshBuilder.CreateGround(
        "gd",
        {
          width: 200,
          height: 200,
          subdivisions: 60,
          updatable: true
        },
        this.scene
      );

      let generator = new HillyValleyGenerator();

      var computeGround = (noiseOffset, Zoffset) => positions => {
        for (var idx = 0; idx < positions.length; idx += 3) {
          const x = positions[idx + 0];
          const z = positions[idx + 2];
          let y = generator.getHeightAt(x, z + Zoffset, noiseOffset);

          positions[idx + 0] = x;
          positions[idx + 1] = y;
          positions[idx + 2] = z;
        }
      };

      var noiseOffset = 0;
      ground.updateMeshPositions(computeGround(noiseOffset, 0));
      ground.updateCoordinateHeights();

      var grid = new BABYLON.GridMaterial("groundMaterial", this.scene);
      grid.lineColor = new BABYLON.Color3(1, 1, 1);
      grid.gridRatio = 200 / 60;
      grid.majorUnitFrequency = 1;

      ground.material = grid;

      this.scene.clearColor = new BABYLON.Color3(69 / 255, 40 / 255, 60 / 255);
      this.scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
      this.scene.fogColor = new BABYLON.Color3(69 / 255, 40 / 255, 60 / 255);
      this.scene.fogStart = 40.0;
      this.scene.fogEnd = 180.0;
      this.scene.fogDensity = 0.01;

      var cameraPos = 0;
      var Zoffset = 0;

      this.scene.registerBeforeRender(() => {
        noiseOffset += 0.03;
        cameraPos = cameraPos + 0.08;
        if (cameraPos > 200 / 60) {
          cameraPos -= 200 / 60;
          Zoffset -= 200 / 60;
        }
        ground.updateMeshPositions(computeGround(noiseOffset, Zoffset));
        ground.updateCoordinateHeights();
        this.camera.setPosition(new BABYLON.Vector3(0, 10, 100 - cameraPos));
        this.camera.setTarget(new BABYLON.Vector3(0, 0, 0 - cameraPos));
      });

      this.engine.runRenderLoop(this.getRenderLoop());

      window.addEventListener("resize", this.getWindowResizeListener());
    }

    getRenderLoop() {
      let render = this;
      return function() {
        render.scene.render();
      };
    }

    getWindowResizeListener() {
      let render = this;
      return function() {
        render.calcOrthoCameraBoundaries();
        threnderis.engine.resize();
      };
    }

    calcOrthoCameraBoundaries() {
      let ratio = this.canvas.clientWidth / this.canvas.clientHeight;
      this.camera.orthoTop = Math.abs(this.zoom);
      let newWidth = this.zoom * ratio;
      this.camera.orthoLeft = -Math.abs(newWidth);
      this.camera.orthoRight = newWidth;
      this.camera.orthoBottom = -Math.abs(this.zoom);
    }
  }

  page = document.getElementById("page");
  page.style.overflow = "hidden";

  document.getElementById("canvas_blend").style.display = "block";
  document.getElementById("loading").style.display = "none";

  window.render = new Render();
})();
